import Vue from 'vue'
import App from './App.vue'
import { treeGetters } from './modules/treeGetters.js'
import decorations from './modules/decorations.js'
import decorator from './modules/decorator.js'
import decorationTypes from './modules/decorationTypes.js'
import historyCapture from './modules/historyCapture.js'
import history from './modules/history.js'
import Vuex from 'vuex'

Vue.use(Vuex)

Vue.config.productionTip = false

const store = new Vuex.Store({
  plugins: [historyCapture],
  modules: {
    tree: treeGetters,
    decorations,
    decorator,
    decorationTypes,
    history
  }
})

new Vue({
  render: function (h) { return h(App) }, store
}).$mount('#app')
