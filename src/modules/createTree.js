import { combine, combineArray, degToRad, rotateInside, addPoints, applyTransformationsArr, rotatePointOrigin, randomise } from './utils.js'

const standardCylinderRadius = 0.01

function createUnitCylinder () {
  const numSections = 6
  const angle = degToRad(360 / numSections)
  const points = []
  let indices = []
  const numPoints = numSections * 2
  for (let i = 0; i < numSections; i++) {
    const currAngle = i * angle
    const x = Math.sin(currAngle)
    const z = Math.cos(currAngle)
    const yb = 0
    const yt = 1
    points.push([x, yb, z])
    points.push([x, yt, z])
    const p = i * 2
    let square = [p + 3, p + 1, p, p + 2, p + 3, p]
    square = square.map(p => rotateInside(p, numPoints))
    const capTri = [rotateInside(p + 3, numPoints), 'cap', rotateInside(p + 1, numPoints)]
    indices.push(...square)
    indices.push(...capTri)
  }

  points.push([0, 1.02, 0])
  const capIndex = points.length - 1

  indices = indices.map(x => {
    if (x === 'cap') {
      return capIndex
    } else {
      return x
    }
  })
  return { indices, points }
}

const scaleUnitCylinder = (points, length, radius) => {
  return points.map(point => {
    point = multiplyPoints(point, [radius, 1, radius])
    if (point[1] > 0) {
      point = addPoints(point, [0, length - 1, 0])
    }
    return point
  })
}

const pineSpace = () => randomise(0.01, 0.005)
const pineLength = () => randomise(0.04, 0.01)
const getNumPoints = () => Math.floor(randomise(5, 3))
const pineBaseHeight = () => randomise(0.006, 0.002)
const pineBaseStraightness = () => randomise(0, 0.01)
const pineTipHeight = () => randomise(0.026, 0.02)
const pineAngleChange = () => randomise(60, 30)

const createPine = (cylinderRadius) => {
  const points = [
    [0, 0, 0],
    [
      pineBaseStraightness(),
      pineBaseHeight(),
      pineBaseStraightness()
    ]
  ]

  const indices = []

  const numPoints = getNumPoints()

  const angle = 360 / numPoints

  for (let i = 0; i < numPoints; i++) {
    points.push(rotatePointOrigin([0, pineTipHeight(), pineLength()], 'y', angle * i))
    indices.push(0, 1, i + 2)
  }

  return { points, indices }
}

const createPines = (length) => {
  const pines = []
  let angle = 0

  for (let i = 0; i < length; i += pineSpace()) {
    const pine = createPine(0.01)
    angle += pineAngleChange()

    const pineTransforms = [
      point => rotatePointOrigin(point, 'y', angle),
      point => addPoints(point, [0, i, 0])
    ]

    pine.points = applyTransformationsArr(pine.points, pineTransforms)
    pines.push(pine)
  }
  return combineArray(pines)
}

const multiplyPoints = (a, b) => [a[0] * b[0], a[1] * b[1], a[2] * b[2]]

function createCylinder (height, radius) {
  const cylinder = createUnitCylinder()
  cylinder.points = scaleUnitCylinder(cylinder.points, height, radius)
  return cylinder
}

const getSubBranchTransforms = (angle, height) => {
  const rot = point => rotatePointOrigin(point, 'x', angle)
  const add = point => addPoints(point, [0, height, 0])
  return [rot, add]
}

const trunkPineGeo = height => createPines(height)

const branchGeo = (branches) => {
  return combineArray(branches.map((branch, i) => {
    let branchGeo = createCylinder(branch.length, standardCylinderRadius)

    const subBranches = branch.subBranches.map((subBranch) => {
      const subBranchGeo = createCylinder(subBranch.length, standardCylinderRadius)
      subBranchGeo.points = applyTransformationsArr(subBranchGeo.points, getSubBranchTransforms(subBranch.angle, subBranch.height))
      return subBranchGeo
    })

    const branchTransforms = [
      point => rotatePointOrigin(point, 'z', branch.branchAngle),
      point => rotatePointOrigin(point, 'y', branch.angle),
      point => addPoints(point, [0, branch.height, 0])
    ]

    branchGeo = combine(branchGeo, combineArray(subBranches))
    branchGeo.points = applyTransformationsArr(branchGeo.points, branchTransforms)
    return branchGeo
  }))
}

const pineGeo = branches => {
  return combineArray(branches.map((branch, i) => {
    let branchGeo = createPines(branch.length, standardCylinderRadius)

    const subBranches = branch.subBranches.map((subBranch) => {
      const subBranchGeo = createPines(subBranch.length, standardCylinderRadius)
      subBranchGeo.points = applyTransformationsArr(subBranchGeo.points, getSubBranchTransforms(subBranch.angle, subBranch.height))
      return subBranchGeo
    })

    const branchTransforms = [
      point => rotatePointOrigin(point, 'z', branch.branchAngle),
      point => rotatePointOrigin(point, 'y', branch.angle),
      point => addPoints(point, [0, branch.height, 0])
    ]

    branchGeo = combine(branchGeo, combineArray(subBranches))

    branchGeo.points = applyTransformationsArr(branchGeo.points, branchTransforms)

    return branchGeo
  }))
}

const createStarGeo = (pointNum, innerRadius, outerRadius) => {
  const outerPoint = [0, -outerRadius, 0]
  const innerPoint = [0, -innerRadius, 0]

  const innerPoints = []
  const outerPoints = []

  for (let i = 0; i < pointNum; i++) {
    const angle = 360 / pointNum * i
    const between = angle - 360 / (pointNum * 2)
    outerPoints.push(rotatePointOrigin(outerPoint, 'z', between))
    innerPoints.push(rotatePointOrigin(innerPoint, 'z', angle))
  }

  const front = [0, 0, 0.02]
  const back = [0, 0, -0.02]

  const points = [...innerPoints, ...outerPoints, front, back].map(x => addPoints(x, [0, innerRadius, 0]))

  const indices = []

  const frontIndex = (pointNum * 2)
  const backIndex = (pointNum * 2) + 1

  for (let i = 0; i < pointNum; i++) {
    const outerPoint = i + pointNum
    const rightInnerPoint = i
    const leftInnerPoint = rotateInside(i - 1, pointNum)
    indices.push(outerPoint, rightInnerPoint, frontIndex)
    indices.push(outerPoint, frontIndex, leftInnerPoint)
    indices.push(outerPoint, leftInnerPoint, backIndex)
    indices.push(outerPoint, backIndex, rightInnerPoint)
  }

  return { points, indices }
}

const treeGeo = (branches, height) => {
  const trunkCylinder = createCylinder(height, standardCylinderRadius)
  const tree = combine(trunkCylinder, branchGeo(branches))
  const moveTreePointsDown = point => addPoints(point, [0, (-height / 2), 0])
  tree.points = tree.points.map(moveTreePointsDown)
  return tree
}

const treePineGeo = (branches, height) => {
  const tree = combine(trunkPineGeo(height), pineGeo(branches))
  const moveTreePointsDown = point => addPoints(point, [0, (-height / 2), 0])
  tree.points = tree.points.map(moveTreePointsDown)
  return tree
}

const starGeo = (height, points, inner, outer) => {
  const star = createStarGeo(points, inner, outer)
  const moveTreePointsDown = point => addPoints(point, [0, (height / 2), 0])
  star.points = star.points.map(moveTreePointsDown)
  return star
}

export {
  treeGeo,
  treePineGeo,
  starGeo
}
