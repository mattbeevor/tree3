const initalState = {
  log: [],
  undone: [],
  replayingMutations: false
}

const continueList = [
  'createEmptyTinselPiece',
  'selectTinsel',
  'reverseTinsel'
]

const history = {
  state: { ...initalState },
  mutations: {
    record (state, payload) {
      state.log.push(payload)
    },
    removeLastRecord (state) {
      let removed
      do {
        removed = state.log.pop()
        if (removed === undefined) {
          break
        }
        state.undone.push(removed)
      } while (removed !== undefined && continueList.includes(removed.type))
    },
    setLog (state, payload) {
      state.log = payload
    },
    setReplayingMutations (state, payload) {
      state.replayingMutations = payload
    },
    removeLastUndone (state) {
      state.undone.pop()
    }
  },
  actions: {
    undo (context, payload) {
      context.commit('setReplayingMutations', true)
      context.commit('resetTree')
      context.commit('resetDecorations')
      context.commit('removeLastRecord')
      context.state.log.forEach(mut => {
        context.commit(mut.type, mut.payload)
      })
      context.commit('setReplayingMutations', false)
    },
    redo (context, payload) {
      let toRedo
      do {
        toRedo = context.state.undone[context.state.undone.length - 1]
        if (toRedo === undefined) {
          break
        }
        context.commit('removeLastUndone')
        context.commit(toRedo.type, toRedo.payload)
      } while (continueList.includes(toRedo.type))
    }
  },
  getters: {
    noUndos: state => state.log.length === 0,
    noRedos: state => state.undone.length === 0
  }
}

export default history
