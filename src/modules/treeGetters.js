import { normaliseUsingLength, rotatePointOrigin, createId, interp, applyTransformations, addPoints } from './utils.js'

const minHeight = 0.5
const maxHeight = 3

const getInitialState = () => ({
  height: 1.6,
  heightStep: 0.35,
  branchStartLength: 0.3,
  branchWidthDepth: 0.3,
  baseGap: 0.2,
  topGap: 0.25,
  tip: 0.27,
  base: 0.2,
  branchSpacing: 0.4,
  subBranchLength: 0.1,
  subBranchSpacing: 0.16,
  subBranchAngle: 45,
  branchAngle: 50,
  baseRadiusProportion: 0.384,
  topRadius: 0.1,
  skipSubBranches: false,
  randomiseDisabled: true,
  minBranchesPerLevel: 4
})

const treeGetters = {
  state: { ...getInitialState() },
  mutations: {
    setHeight (state, change) {
      state.height = Math.min(maxHeight, Math.max(minHeight, state.height + change))
    },
    resetTree (state) {
      Object.assign(state, getInitialState())
    }
  },
  getters: {
    baseRadius: state => state.height * state.baseRadiusProportion,
    levels: (state, getters) => {
      const { base, height, tip, baseGap, topGap, topRadius } = state
      const levels = []
      const branchHeightRange = height - (base + tip)
      const stepNum = Math.round(branchHeightRange / ((baseGap + topGap) / 2))
      const step = 1 / stepNum

      for (let i = 1; i > -0.0001; i -= step) {
        levels.push({
          height: interp(base, height - tip, i),
          length: interp(getters.baseRadius, topRadius, i)
        })
      }

      return levels
    },
    branches: (state, getters) => {
      const branches = []
      let evenLevel = false
      const { branchAngle, minBranchesPerLevel, height, branchSpacing } = state
      const { levels } = getters
      const treeHeight = height
      levels.forEach(level => {
        const { length, height } = level
        evenLevel = !evenLevel
        const circumference = Math.PI * length * 2
        const branchesOnLevel = Math.max(minBranchesPerLevel, Math.floor(circumference / branchSpacing))
        const angleBetweenBranches = 360 / branchesOnLevel
        const startAngle = evenLevel ? 0 : angleBetweenBranches / 2
        for (let index = 0; index < branchesOnLevel; index++) {
          const angle = startAngle + (angleBetweenBranches * index)
          const moveToLevel = point => addPoints(point, [0, height - treeHeight / 2, 0])
          const tipTransforms = [
            point => addPoints(point, [0, length, 0]),
            point => rotatePointOrigin(point, 'z', branchAngle),
            point => rotatePointOrigin(point, 'y', angle),
            moveToLevel
          ]
          const base = moveToLevel([0, 0, 0])
          const tip = applyTransformations([0, 0, 0], tipTransforms)
          const clockwiseVector = applyTransformations([0, 0, 1], [
            ...tipTransforms,
            point => addPoints(point, [-tip[0], -tip[1], -tip[2]])
          ])
          const normal = normaliseUsingLength(base, tip, length)
          const id = createId(angle, index, height, length)
          const { subBranchAngle, subBranchSpacing } = state
          const subBranches = []

          for (let i = subBranchSpacing; i < length - subBranchSpacing; i += subBranchSpacing) {
            const lengthAlongBranch = i
            const subBranchlength = (length - i) * 0.3 // subBranchLength
            const angle = subBranchAngle
            subBranches.push(
              { height: lengthAlongBranch, angle, length: subBranchlength },
              { height: lengthAlongBranch, angle: -angle, length: subBranchlength }
            )
          }

          branches.push({ angle, branchAngle, height, length, tip, base, normal, clockwiseVector, id, subBranches })
        }
      })
      return branches
    }
  }
}

export { treeGetters }
