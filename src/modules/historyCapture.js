const ignoreList = [
  'record',
  'setReplayingMutations',
  'setMode',
  'setInteractionMode',
  'new',
  'removeLastUndone'
]

const historyCapture = store => {
  store.subscribe((mutation, state) => {
    if (ignoreList.includes(mutation.type) || state.history.replayingMutations) {
      return
    }
    store.commit('record', mutation)
  })
}

export default historyCapture
