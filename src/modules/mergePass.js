// Source https://codesandbox.io/s/twilight-feather-myhm9

import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js'
import * as THREE from 'three'
function mergePass (renderTarget) {
  return new ShaderPass(
    new THREE.ShaderMaterial({
      uniforms: { tDiffuse: { value: null }, bloomTexture: { value: renderTarget } },
      vertexShader: `varying vec2 vUv;
          void main() {
            vUv = uv;
            gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
          }`,
      fragmentShader: `uniform sampler2D tDiffuse;
          uniform sampler2D bloomTexture; 
          varying vec2 vUv; 
          vec4 getTexture( sampler2D texelToLinearTexture ) { 
            return mapTexelToLinear( texture2D( texelToLinearTexture , vUv ) ); 
          }
          void main() {
            gl_FragColor = ( getTexture(tDiffuse) + vec4( 1.0 ) * getTexture( bloomTexture ) );
          }`
    })
  )
}

export {
  mergePass
}
