import { createId } from './utils.js'

function isClockwise (angle1, angle2) {
  const difference = angle1 - angle2

  if (difference === -180) {
    return true
  }
  if (difference === 180) {
    return false
  }

  return ((difference < -180) || (difference > 0 && difference < 180))
}

const getInitialState = () => ({
  baubles: [],
  tinsels: [{
    typeIndex: 0,
    depth: 0.15,
    branchIndices: []
  }],
  star: {
    exists: false,
    points: 5,
    inner: 0.05,
    outer: 0.1
  }
})

const decorations = {
  state: { ...getInitialState() },
  mutations: {
    resetDecorations (state, payload) {
      const emptyState = getInitialState()
      Object.assign(emptyState.tinsels[0], payload)
      Object.assign(state, emptyState)
    },
    removeBauble (state, payload) {
      state.baubles.splice(payload, 1)
    },
    changeBauble (state, payload) {
      state.baubles[payload.index].typeIndex = payload.typeIndex
    },
    toggleStar (state) {
      state.star.exists = !state.star.exists
    },
    changeStarPoints (state, payload) {
      state.star.points = Math.max(3, Math.min(state.star.points + payload, 18))
    },
    changeStarInnerRadius (state, payload) {
      state.star.inner = Math.max(0.01, Math.min(state.star.inner + payload, 0.3))
    },
    changeStarOuterRadius (state, payload) {
      state.star.outer = Math.max(0.01, Math.min(state.star.outer + payload, 0.3))
    },
    addBauble (state, payload) {
      state.baubles.push({
        branchIndex: payload.branchIndex,
        typeIndex: payload.typeIndex
      })
    },
    createTinselPiece (state, payload) {
      state.tinsels.push({
        typeIndex: payload.typeIndex,
        depth: payload.depth,
        branchIndices: [payload.branchIndex]
      })
    },
    createEmptyTinselPiece (state, payload) {
      state.tinsels.push({
        typeIndex: payload.typeIndex,
        depth: payload.depth,
        branchIndices: []
      })
    },
    removeTinsel (state, payload) {
      state.tinsels.splice(payload, 1)
    },

    addTinsel (state, payload) {
      state.tinsels[payload.tinselIndex].branchIndices.push(payload.branchIndex)
    },
    setTinselType (state, payload) {
      state.tinsels[payload.tinselIndex].typeIndex = payload.typeIndex
    },
    changeTinselDepth (state, payload) {
      const depth = Math.max(0, state.tinsels[payload.tinselIndex].depth + payload.change)
      state.tinsels[payload.tinselIndex].depth = depth
    },
    reverseTinsel (state, payload) {
      state.tinsels[payload].branchIndices.reverse()
    },
    removeLastTinselPiece (state, payload) {
      state.tinsels[payload].branchIndices.pop()
    }
  },
  getters: {
    starId: (state) => {
      return createId(...Object.entries(state.star))
    },

    baubleSetsWithIds: (state, getters, rootState, rootGetters) => {
      const height = rootState.tree.height

      const sets = {}

      const maxBranchIndex = rootGetters.branches.length - 1

      const baubles = state.baubles.filter(x => x.branchIndex <= maxBranchIndex)

      baubles.forEach(bauble => {
        if (!sets[bauble.typeIndex]) {
          sets[bauble.typeIndex] = {
            branchIndices: [],
            typeIndex: bauble.typeIndex
          }
        }
        sets[bauble.typeIndex].branchIndices.push(bauble.branchIndex)
      })

      const setsWithIds = Object.values(sets).map(set => (
        {
          ...set,
          id: createId(set.typeIndex, set.branchIndices.join('-'), height)
        })
      )

      return setsWithIds
    },

    tinselDirections: (state, getters, rootState, rootGetters) => {
      const branches = rootGetters.branches
      const maxBranchIndex = branches.length - 1
      return state.tinsels.map((tinsel, i) => {
        const branchIndices = tinsel.branchIndices.filter(x => x <= maxBranchIndex)

        /*
          The two loops hanging down from a branch that a piece of tinsel is hung on hang down from different sides
          of the branch. This means the starting and ending points of each large loop depend on whether it is joining
          the branch from the clockwise side or the other side, we therefore determine if each branch being crossed
          is clockwise or anticlockwise. We think of the tinsel as being a directional path starting at the first position added.
        */

        return branchIndices.map((branchIndex, i) => {
          const branch = branches[branchIndices[i]]
          const prevBranch = branches[branchIndices[i - 1]]
          const nextBranch = branches[branchIndices[i + 1]]

          if (!nextBranch && !prevBranch) {
            return false
          }

          if (!nextBranch) {
            return !isClockwise(branch.angle, prevBranch.angle)
          }

          if (!prevBranch) {
            return isClockwise(branch.angle, nextBranch.angle)
          }

          const clockWiseToPrev = isClockwise(branch.angle, prevBranch.angle)
          const clockWiseToNext = isClockwise(branch.angle, nextBranch.angle)
          // The two adjacent branches are on opposite sides
          // The direction of flow is whatever direction the next branch is in
          if (clockWiseToNext !== clockWiseToPrev) {
            return clockWiseToNext
          } else {
            // Whichever one is higher gets to be on the closer side
            const nextBranchHigherThanPrevBranch = nextBranch.tip[1] > prevBranch.tip[1]
            return clockWiseToNext === nextBranchHigherThanPrevBranch
          }
        })
      })
    },
    tinselCurvesWithIds: (state, getters, rootState, rootGetters) => {
      const height = rootState.tree.height
      const maxBranchIndex = rootGetters.branches.length - 1

      return state.tinsels.reduce((acc, tinsel, i) => {
        const branchIndices = tinsel.branchIndices.filter(x => x <= maxBranchIndex)

        if (branchIndices.length === 0) {
          return acc
        }

        const typeIndex = tinsel.typeIndex
        let id = 'tinsel'
        const curves = []
        const depth = tinsel.depth

        branchIndices.forEach((branchIndex, j) => {
          const branchClockwise = getters.tinselDirections[i][j]

          id = id + createId(depth, tinsel.typeIndex, 'over', branchIndex)

          curves.push({
            curveType: 'over',
            clockwise: branchClockwise,
            branchIndex: branchIndex
          })

          const last = j === branchIndices.length - 1

          if (last) {
            return
          }

          const nextBranchIndex = branchIndices[j + 1]
          const nextBranchClockwise = !getters.tinselDirections[i][j + 1]

          id = id + createId(depth, tinsel.typeIndex, 'b', branchIndex, branchClockwise, nextBranchIndex, nextBranchClockwise, height)

          curves.push({
            tinselTypeIndex: tinsel.typeIndex,
            curveType: 'between',
            depth: tinsel.depth,
            branches: [
              {
                branchIndex,
                clockwise: branchClockwise
              },
              {
                branchIndex: nextBranchIndex,
                clockwise: nextBranchClockwise
              }
            ]
          })
        })
        const selected = rootState.decorator.currentTinselPieceIndex === i
        return [...acc, { curves, typeIndex, id, selected }]
      }, [])
    },
    groupedTinselCurvesWithIds: (state, getters, rootState, rootGetters) => {
      const groups = {}

      getters.tinselCurvesWithIds.forEach(obj => {
        const key = `key${obj.typeIndex}`
        if (!groups[key]) {
          groups[key] = {
            typeIndex: obj.typeIndex,
            id: obj.id,
            curves: [obj.curves],
            ids: [obj.id],
            selected: [obj.selected]
          }
        } else {
          groups[key].curves.push(obj.curves)
          groups[key].id = groups[key].id + obj.id
          groups[key].ids.push(obj.id)
          groups[key].selected.push(obj.selected)
          // allows you to create seperate highlight things that can be selected individually,
          // this will be the only thing that gets casted onto
        }
      })

      return Object.values(groups)
    },
    treeIsClear: state => state.baubles.length === 0 && state.tinsels.length < 2 && state.tinsels[0].branchIndices.length === 0 && !state.star.exists
  },
  actions: {
    clearEmptyTinsels (context) {
      const empty = context.state.tinsels.findIndex(x => x.branchIndices.length === 0)
      if (empty > -1) {
        context.commit('removeTinsel', empty)
        context.dispatch('clearEmptyTinsels')
      }
    }
  }
}

export default decorations
