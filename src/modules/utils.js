const degToRad = x => x * 0.0174533

const radToDeg = x => x * 57.2958

const lastOf = (arr) => arr[arr.length - 1]

const addPoints = (a, b) => [a[0] + b[0], a[1] + b[1], a[2] + b[2]]

function moveDown (y, height) {
  return y - height / 2
}

function interp (from, to, pos) {
  return (from * (1 - pos)) + (to * pos)
}

const rotateInside = (pos, range) => {
  return (pos + range) % range
}

const randomise = (x, amount) => x + ((Math.random() - 0.5) * amount)

const onlyInFirstArray = (arr1, arr2, field) => {
  const arr2Field = arr2.map(x => x[field])
  return arr1.filter(x => !arr2Field.includes(x[field]))
}

const rotatePointOrigin = (point, axis, angle) => {
  const xys = { x: [1, 2, 0], y: [0, 2, 1], z: [0, 1, 2] }
  const xy = xys[axis]
  const cos = Math.cos(degToRad(angle))
  const sin = Math.sin(degToRad(angle))
  const n = [0, 0, 0]
  n[xy[0]] = cos * point[xy[0]] - sin * point[xy[1]]
  n[xy[1]] = sin * point[xy[0]] + cos * point[xy[1]]
  n[xy[2]] = point[xy[2]]
  return n
}

const range = (x) => Array.from(Array(x).keys())

function combine (shape1, shape2) {
  const indicesOffset = shape1.points.length
  const offSetIndices = shape2.indices.map(x => x + indicesOffset)
  return {
    points: [...shape1.points, ...shape2.points],
    indices: [...shape1.indices, ...offSetIndices]
  }
}

function combineArray (shapes) {
  let points = []
  let indices = []
  shapes.forEach(shape => {
    const indicesOffset = points.length
    const offSetIndices = shape.indices.map(x => x + indicesOffset)
    points = [...points, ...shape.points]
    indices = [...indices, ...offSetIndices]
  })
  return { points, indices }
}

function createId (...args) {
  return args.map(x => {
    switch (x) {
      case true:
        return 1
      case false:
        return 0
      default:
        return x
    }
  }).join('_')
}

function applyTransformations (point, transformations) {
  return transformations.reduce((a, x) => x(a), point)
}

function applyTransformationsArr (points, transformations) {
  return points.map(point => applyTransformations(point, transformations))
}

const normaliseUsingLength = (a, b, length) => a.map((Ad, i) => (b[i] - Ad) / length)

const scalePoint = (point, scale) => point.map(x => x * scale)

export {
  moveDown,
  interp,
  rotateInside,
  rotatePointOrigin,
  degToRad,
  radToDeg,
  combine,
  combineArray,
  range,
  randomise,
  onlyInFirstArray,
  lastOf,
  createId,
  addPoints,
  applyTransformations,
  applyTransformationsArr,
  normaliseUsingLength,
  scalePoint
}
