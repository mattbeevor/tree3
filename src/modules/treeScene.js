import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls'
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js'
import { UnrealBloomPass } from 'three/examples/jsm/postprocessing/UnrealBloomPass.js'
import { CopyShader } from 'three/examples/jsm/shaders/CopyShader.js'
import { FXAAShader } from 'three/examples/jsm/shaders/FXAAShader.js'
import { SMAAPass } from 'three/examples/jsm/postprocessing/SMAAPass.js'
import { mergePass } from './mergePass.js'
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js'
import baubleTypes from '../assets/baubleTypes.json'
import tinselTypes from '../assets/tinselTypes.json'
import { rotatePointOrigin, combineArray, range, randomise, onlyInFirstArray, scalePoint, addPoints } from './utils'
import { treeGeo, treePineGeo, starGeo } from './createTree'

const branchTipRadius = 0.05
const tinselCastLinePrecision = 0.1
const minDepth = 0.1
const scaleHide = object => object.scale.set(0.00001, 0.00001, 0.00001)

function createQuads (name, shape, material, parent) {
  const geometry = new THREE.BufferGeometry()
  geometry.setAttribute(
    'position',
    new THREE.BufferAttribute(new Float32Array(shape.points.flat()), 3)
  )
  geometry.setIndex(shape.indices)
  geometry.computeFaceNormals()
  geometry.computeVertexNormals()
  const mesh = new THREE.Mesh(geometry, material)
  mesh.name = name
  parent.add(mesh)
}

function cubicBezier (a, b, c, d) {
  return new THREE.CubicBezierCurve3(
    new THREE.Vector3(...a),
    new THREE.Vector3(...b),
    new THREE.Vector3(...c),
    new THREE.Vector3(...d)
  )
}

function dispose () {
  [...arguments].forEach(x => x.dispose)
}

function makeRectangularHull (end1, end2, offset) {
  return [
    end1,
    addPoints(end1, [0, offset, 0]),
    addPoints(end2, [0, offset, 0]),
    end2
  ]
}

function makeCurveHull (end1, end2, offset) {
  const lowerBase = Math.min(end1[1] + offset, end2[1] + offset)
  const end1Base = [end1[0], lowerBase, end1[2]]
  const end2Base = [end2[0], lowerBase, end2[2]]

  return [
    end1,
    end1Base,
    end2Base,
    end2
  ]
}

function getMousePosFromClick (el, event) {
  const clickleft = event.clientX - el.offsetLeft
  const clicktop = event.clientY - el.offsetTop
  const clickleftRel = clickleft / el.offsetWidth
  const clicktopRel = clicktop / el.offsetHeight
  const g = { x: clickleftRel * 2 - 1, y: -clicktopRel * 2 + 1 }
  return g
}

function getCurvePathLength (curvePath) {
  return curvePath.getCurveLengths().slice(-1)[0]
}

function getCamera (fov, aspect, near, far, x, y, z) {
  const camera = new THREE.PerspectiveCamera(30, aspect, near, far)
  camera.position.x = x
  camera.position.y = y
  camera.position.z = z
  return camera
}

function createTinselBufferGeometry () {
  const geometry = new THREE.BufferGeometry()

  var baseVertices = [
    [-0.03, -0.003, -0.003],
    [-0.03, 0.003, 0.003],
    [0.03, 0, 0]
  ]

  const numStrands = 7
  const angleChange = 360 / numStrands

  const vertices = combineArray(range(numStrands).map(i => {
    const angle = i * angleChange
    const points = baseVertices.map(p => rotatePointOrigin(p, 'z', angle))
    return { indices: [0, 1, 2], points }
  }))

  geometry.setAttribute(
    'position',
    new THREE.BufferAttribute(new Float32Array(vertices.points.flat()), 3)
  )

  geometry.setIndex(vertices.indices)
  geometry.computeVertexNormals()

  return geometry
}

function curveToGeometry (interval, curvePaths, geometry, material, eye, lookAt, offset) {
  const pathNumber = curvePaths.reduce((a, curvePath) => a + curvePath.curves.reduce((b, curve) => b + Math.ceil(curve.getLength() / interval) + 1, 0), 0)

  const mesh = new THREE.InstancedMesh(geometry, material, pathNumber)
  let curr = 0

  curvePaths.forEach(curvePath => {
    curvePath.curves.forEach(curve => {
      const number = Math.ceil(curve.getLength() / interval)
      const e = 0.1 / number
      const start = offset || 0
      for (let i = start; i <= number; i++) {
        eye.up.set(randomise(0, 10), 10, randomise(0, 10))
        const u = i / number
        const uOffset = u < 0.5 ? u + e : u - e
        curve.getPointAt(u, eye.position)
        curve.getPointAt(uOffset, lookAt)
        eye.lookAt(lookAt)
        eye.updateMatrix()
        mesh.setMatrixAt(curr, eye.matrix)
        curr++
      }
    })
  })

  mesh.instanceMatrix.needsUpdate = true
  return mesh
}

function getDepthsToTry (depth) {
  // TODO memoize this
  const attempts = 8
  const variance = 0.02
  const arr = [depth]
  for (let i = 1; i <= attempts; i++) {
    const offset = variance * i
    arr.push(depth + offset, depth - offset)
  }

  const filtered = arr.filter(x => x > minDepth)
  return filtered
}

function getBranchCurve2 (branch1, branch2, clockwise1, clockwise2, depth, loopRadius) {
  const pos1 = addPoints(branch1.tip, scalePoint(branch1.clockwiseVector, (clockwise1 ? 1 : -1) * loopRadius))
  const pos2 = addPoints(branch2.tip, scalePoint(branch2.clockwiseVector, (clockwise2 ? 1 : -1) * loopRadius))
  return cubicBezier(...makeCurveHull(pos1, pos2, -depth))
}
// TODO above and below should be one function
function getBranchCurve (branch1, branch2, clockwise1, clockwise2, depth, loopRadius) {
  const pos1 = addPoints(branch1.tip, scalePoint(branch1.clockwiseVector, (clockwise1 ? 1 : -1) * loopRadius))
  const pos2 = addPoints(branch2.tip, scalePoint(branch2.clockwiseVector, (clockwise2 ? 1 : -1) * loopRadius))
  return cubicBezier(...makeRectangularHull(pos1, pos2, -depth))
}

function createSceneLights (group) {
  const dLight1 = new THREE.DirectionalLight(0xffddff, 1)
  const dLight2 = new THREE.DirectionalLight(0xeeffee, 1)
  dLight2.position.set(-3, 3, 0)
  dLight1.position.set(3, 0, 0)
  const ambientLight = new THREE.AmbientLight(0x888888)
  group.add(ambientLight, dLight1, dLight2)
  group.traverse(x => x.updateMatrix())
}

function createMaterials () {
  return {
    branchTip: new THREE.MeshBasicMaterial({ color: 0xffaa00, opacity: 0.1, transparent: true }),
    star: new THREE.MeshPhongMaterial({ color: 0xffff55 }),
    hover: new THREE.MeshBasicMaterial({ color: 0xffaa00, opacity: 0.4, transparent: true }),
    highlight: new THREE.MeshLambertMaterial({ color: 0xaaff33, opacity: 0.3, transparent: true }),
    hiddenHighlight: new THREE.MeshBasicMaterial({ visible: false }),
    tree: new THREE.MeshLambertMaterial({ color: 0x229222 }),
    wire: new THREE.MeshLambertMaterial({ color: 0x332222 }),
    pine: new THREE.MeshLambertMaterial({ color: 0x229222, side: THREE.DoubleSide }),
    checkCurve: new THREE.LineBasicMaterial({ color: 0xff0000 })
  }
}

function removeFromObjectByNameAndDispose (name, parent) {
  const child = parent.getObjectByName(name)
  if (child) {
    parent.remove(child)
    dispose(child)
  }
}

function curvePathToTube (curvePath, material, radius) {
  const interval = 0.02
  const number = Math.ceil(getCurvePathLength(curvePath) / interval)
  const tubeGeom = new THREE.TubeBufferGeometry(curvePath, number, radius, 5, false)
  return new THREE.Mesh(tubeGeom, material)
}

function createCane () {
  const path = new THREE.CatmullRomCurve3([
    new THREE.Vector3(-0.02, -0.06, 0),
    new THREE.Vector3(-0.02, 0.0, 0),
    new THREE.Vector3(-0.02, 0.03, 0),
    new THREE.Vector3(-0.01, 0.045, 0),
    new THREE.Vector3(0.01, 0.045, 0),
    new THREE.Vector3(0.02, 0.03, 0),
    new THREE.Vector3(0.02, 0.02, 0)
  ])
  return new THREE.TubeBufferGeometry(path, 20, 0.005, 8, false)
}

function createLongBaubleShape () {
  const points = [
    new THREE.Vector2(0, -0.05),
    new THREE.Vector2(0.009, -0.025),
    new THREE.Vector2(0.0125, 0),
    new THREE.Vector2(0.011, 0.025),
    new THREE.Vector2(0, 0.05)
  ]
  return new THREE.LatheBufferGeometry(points)
}

function getCaneTexture () {
  const image = new Image()
  image.src = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAYAAABytg0kAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAVSURBVBhXY/gPBG9lVP4zMYABAwMAVsEGKxDeq54AAAAASUVORK5CYII='
  const texture = new THREE.Texture()
  texture.image = image
  image.onload = function () {
    texture.needsUpdate = true
  }
  texture.rotation = 0.34
  texture.repeat.set(20, 20)
  texture.wrapS = texture.wrapT = THREE.MirroredRepeatWrapping
  texture.magFilter = THREE.NearestFilter
  texture.minFilter = THREE.NearestFilter
  return texture
}

const baubleMaterialCreators = {
  stripe: () => new THREE.MeshLambertMaterial({ map: getCaneTexture() }),
  shinyRed: () => new THREE.MeshPhongMaterial({ color: 0xff3333 }),
  shinyBlue: () => new THREE.MeshPhongMaterial({ color: 0x3333ff }),
  silver: () => new THREE.MeshPhongMaterial({ color: 0xcccccc })
}

const baubleGeometryCreators = {
  cane: createCane,
  sphere: () => new THREE.SphereBufferGeometry(0.04),
  long: createLongBaubleShape
}

const tinselMaterialCreators = {
  pinkLight: () => new THREE.MeshBasicMaterial({ color: 0xff1111, side: THREE.DoubleSide }),
  blueLight: () => new THREE.MeshBasicMaterial({ color: 0x1111ff, side: THREE.DoubleSide }),
  greenLight: () => new THREE.MeshBasicMaterial({ color: 0x11ff11, side: THREE.DoubleSide }),
  silver: () => new THREE.MeshPhongMaterial({ color: 0xcccccc, side: THREE.DoubleSide }),
  gold: () => new THREE.MeshPhongMaterial({ color: 0xffff66, side: THREE.DoubleSide })
}

const tinselGeometryCreators = {
  bead: () => new THREE.SphereBufferGeometry(0.005, 8, 8),
  light: () => new THREE.SphereBufferGeometry(0.016, 8, 8),
  tinsel: createTinselBufferGeometry
}

const mapOrApply = (x, fArr) => Array.isArray(x) ? x.map(y => fArr[y]()) : fArr[x]()

function setUpObjects (types, shapeFunctions, materialFunctions) {
  types.forEach(type => {
    type.geometry = mapOrApply(type.shapeName, shapeFunctions)
    type.material = mapOrApply(type.materialName, materialFunctions)
  })
}

function makeGroupUnder (name, parent) {
  const group = new THREE.Group()
  group.name = name
  parent.add(group)
  return group
}

function getRenderPassSet (ratio, canvas, scene, camera, renderer) {
  const width = canvas.width
  const height = canvas.height
  const scaleWidth = width * ratio
  const scaleHeight = height * ratio
  const parameters = { minFilter: THREE.LinearFilter, magFilter: THREE.LinearFilter, format: THREE.RGBAFormat, stencilBuffer: false }
  const renderTarget1 = new THREE.WebGLRenderTarget(scaleWidth, scaleHeight, parameters)
  return {
    renderer,
    renderScene: new RenderPass(scene, camera),
    renderTarget1,
    renderTarget2: new THREE.WebGLRenderTarget(scaleWidth, scaleHeight, parameters),
    renderTarget3: new THREE.WebGLRenderTarget(scaleWidth, scaleHeight, parameters),
    mergePass: mergePass(renderTarget1),
    bloomPass: new UnrealBloomPass(new THREE.Vector2(width / 3, height / 3), 2, 0.2, 0.1), // strenght radius threshold
    fxaaPass: new ShaderPass(FXAAShader),
    copyPass: new ShaderPass(CopyShader)
  }
}

export class TreeScene {
  constructor (store) {
    this.timeout = {}
    this.store = store
    this.caster = new THREE.Raycaster()
    this.mouse = new THREE.Vector2()
    this.canvas = document.querySelector('#c')
    this.camera = getCamera(70, this.canvas.clientWidth / this.canvas.clientHeight, 0.2, 20, 1.5, 0, 3.4)
    this.scene = new THREE.Scene()

    this.lights = makeGroupUnder('lights', this.scene)

    createSceneLights(this.lights)

    this.materials = createMaterials()

    setUpObjects(baubleTypes, baubleGeometryCreators, baubleMaterialCreators)
    setUpObjects(tinselTypes, tinselGeometryCreators, tinselMaterialCreators)

    this.branchTips = makeGroupUnder('branchTips', this.scene)
    this.baubles = makeGroupUnder('baubles', this.scene)
    this.tinsels = makeGroupUnder('tinsels', this.scene)
    this.tinselHighlights = makeGroupUnder('tinselHighlights', this.scene)

    this.hovering = false
    this.hoverIndex = 0
    const hoverMesh = new THREE.SphereBufferGeometry(0.06)
    this.hoverItem = new THREE.Mesh(hoverMesh, this.materials.hover)
    this.hoverItem.name = 'hoverItem'
    scaleHide(this.hoverItem)
    this.hoverItem.updateMatrix()
    this.scene.add(
      this.hoverItem
    )

    this.renderer = new THREE.WebGLRenderer({ canvas: this.canvas })
    this.renderer.setSize(window.innerWidth, window.innerHeight)
    this.renderer.toneMapping = THREE.ReinhardToneMapping
    this.renderer.setPixelRatio(1)
    this.renderer.autoClear = false

    this.lqRender = getRenderPassSet(1, this.canvas, this.scene, this.camera, this.renderer)
    this.hqRender = getRenderPassSet(2, this.canvas, this.scene, this.camera, this.renderer)

    this.smaaPass = new SMAAPass(window.innerWidth, window.innerHeight)

    this.controls = new OrbitControls(this.camera, this.canvas)
    this.controls.update()
    this.controls.addEventListener('change', this.animate.bind(this, true))
    this.center = new THREE.Vector3(0, 0, 0)
    this.up = new THREE.Vector3(0, 1, 0)
    this.direction = new THREE.Vector3(0, 0, 0)
    this.start = new THREE.Vector3(0, 1, 0)
    this.eye = new THREE.Object3D()
    this.lookAt = new THREE.Vector3()
    this.checkCurveGeometry = new THREE.BufferGeometry()
    this.checkCurveObject = new THREE.Line(this.checkCurveGeometry, this.materials.checkCurve)
    window.onresize = this.updateRenderer.bind(this)
  }

  updateRenderer () {
    const h = window.innerHeight
    const w = window.innerWidth
    this.camera.aspect = (w / h)
    this.camera.updateProjectionMatrix()
    this.renderer.setSize(w, h)
    this.animate()
  }

  setUpTree (branches, height) {
    const arr = this.branchTips.children.slice()
    this.branchTips.remove(...this.branchTips.children)
    dispose(...arr)

    removeFromObjectByNameAndDispose('tree', this.scene)
    removeFromObjectByNameAndDispose('pine', this.scene)

    createQuads('tree', treeGeo(branches, height), this.materials.tree, this.scene)
    createQuads('pine', treePineGeo(branches, height), this.materials.pine, this.scene)

    this.store.getters.branches.forEach((branch, i) => {
      const branchTipGeo = new THREE.SphereBufferGeometry(branchTipRadius)
      const branchTip = new THREE.Mesh(branchTipGeo, this.materials.branchTip)
      branchTip.index = i
      branchTip.position.set(...branch.tip)
      this.branchTips.add(branchTip)
      branchTip.updateMatrix()
    })

    this.updateStar()
  }

  updateStar () {
    removeFromObjectByNameAndDispose('star', this.scene)
    const { points, inner, outer, exists } = this.store.state.decorations.star

    if (!exists) {
      return
    }

    const height = this.store.state.tree.height
    createQuads('star', starGeo(height, points, inner, outer), this.materials.star, this.scene)
  }

  showGuides (should) {
    this.materials.highlight.visible = should
    this.materials.branchTip.visible = should
    this.animate()
  }

  setCheckCurveGeometry (curve) {
    this.checkCurveGeometry.setFromPoints(curve.getPoints(50))
    this.checkCurveObject.updateMatrix()
  }

  createBauble (typeIndex, branchIndices, name) {
    const baubleType = baubleTypes[typeIndex]
    const mesh = new THREE.InstancedMesh(baubleType.geometry, baubleType.material, branchIndices.length)
    mesh.name = name
    const positions = branchIndices.map(x => this.store.getters.branches[x].tip)
    this.eye.up.set(0, 1, 0)
    positions.forEach((b, i) => {
      const position = [b[0], b[1] + baubleType.offset, b[2]]
      this.eye.position.set(...position)
      this.eye.updateMatrix()
      this.lookAt.set(0, position[1], 0)
      this.eye.lookAt(this.lookAt)
      this.eye.updateMatrix()
      mesh.setMatrixAt(i, this.eye.matrix)
    })
    this.baubles.add(mesh)
  }

  setToBlack () {
    this.lights.children.forEach(light => {
      light.intensity = 0.2
    })
  }

  // TODO above and below should be one function with intensity arg
  setToDefault () {
    this.lights.children.forEach(light => {
      light.intensity = 1
    })
  }

  animate (moveOnly) {
    if (!moveOnly) {
      this.scene.traverse(x => {
        x.matrixAutoUpdate = false
      })
      this.render(this.hqRender)
    } else {
      this.render(this.lqRender)
      clearTimeout(this.timeout)
      this.timeout = setTimeout(() => this.render(this.hqRender), 10)
    }
  }

  render (passSet, aa) {
    this.setToBlack()
    passSet.renderer.setRenderTarget(passSet.renderTarget1)
    passSet.renderer.clear()
    passSet.renderer.clearDepth()
    passSet.renderer.render(this.scene, this.camera)
    passSet.bloomPass.render(passSet.renderer, null, passSet.renderTarget1, 0, false)
    this.setToDefault()
    passSet.renderer.setRenderTarget(passSet.renderTarget2)
    passSet.renderer.clear()
    passSet.renderer.clearDepth()
    passSet.renderer.render(this.scene, this.camera)
    // this.mergePass.renderToScreen = true
    passSet.mergePass.render(passSet.renderer, passSet.renderTarget3, passSet.renderTarget2)
    passSet.copyPass.renderToScreen = true
    passSet.copyPass.render(passSet.renderer, null, passSet.renderTarget3)
  }

  selectTinsel (event) {
    const intersect = this.castToItem(event, this.tinselHighlights.children)
    if (intersect) {
      this.store.dispatch('selectTinsel', intersect.name)
    }
  }

  clickRemoveBauble (event) {
    const object = this.castToItem(event, this.branchTips.children)
    if (object) {
      this.store.dispatch('removeBauble', object.index)
    }
  }

  castToItem (event, arr) {
    this.setCasterToClick(event)
    const intersects = this.caster.intersectObjects(arr)
    if (intersects.length > 0) {
      return intersects[0].object
    }
  }

  hover (event) {
    this.setCasterToClick(event)
    const intersects = this.caster.intersectObjects(this.branchTips.children)
    if (intersects.length > 0) {
      const index = intersects[0].object.index
      if (this.hovering === true && this.hoverIndex === index) {

      } else {
        this.hovering = true
        this.hoverIndex = index
        this.hoverItem.scale.set(1, 1, 1)
        this.hoverItem.position.set(...this.store.getters.branches[index].tip)
        this.hoverItem.updateMatrix()
        this.animate()
      }
    } else {
      if (this.hovering === false) {

      } else {
        this.hovering = false
        scaleHide(this.hoverItem)
        this.hoverItem.updateMatrix()
        this.animate()
      }
    }
  }

  castToTree (event) {
    this.setCasterToClick(event)
    const intersects = this.caster.intersectObjects(this.branchTips.children)
    if (intersects.length > 0) {
      const index = intersects[0].object.index
      this.store.dispatch('touchBranch', index)
    }
  }

  setCasterToClick (event) {
    this.caster.far = 10
    this.mouse = getMousePosFromClick(this.canvas, event)
    this.caster.setFromCamera(this.mouse, this.camera)
  }

  setCasterAndCheckIntersection (start, direction, length, object) {
    this.setDirection(direction)
    this.setStart(start)
    this.caster.params.Line.threshold = tinselCastLinePrecision
    this.caster.near = 0.1
    this.caster.far = length
    this.caster.set(this.start, this.direction)
    return this.caster.intersectObjects([object])
  }

  selectionCurve (curvePath, selected) {
    const material = selected ? this.materials.highlight : this.materials.hiddenHighlight
    return curvePathToTube(curvePath, material, 0.022)
  }

  setDirection (x) {
    this.direction.set(...x)
  }

  setStart (x) {
    this.start.set(...x)
  }

  // TODO - refactor - Different levels of abstraction, too many arguments
  // could this use a while loop instead of recursion?
  getBetweenTinselCurve (branch1, branch2, branches, clockwise1, clockwise2, depths, loopRadius, recursions) {
    const curve = getBranchCurve2(branch1, branch2, clockwise1, clockwise2, depths[recursions], loopRadius)
    this.setCheckCurveGeometry(curve)

    const otherBranches = onlyInFirstArray(branches, [branch1, branch2], 'id')

    const intersectBranch = otherBranches.find((branch, i) => {
      const intersections = this.setCasterAndCheckIntersection(branch.base, branch.normal, branch.length, this.checkCurveObject)
      if (intersections.length > 0) {
        return true
      }
    })

    if (recursions > 50) {
      return false
    }

    if (!intersectBranch) {
      return [curve]
    }

    if (recursions < depths.length - 1) {
      const changed = this.getBetweenTinselCurve(branch1, branch2, branches, clockwise1, clockwise2, depths, loopRadius, recursions + 1)
      if (changed) {
        return changed
      }
    }

    if (recursions === depths.length) {
      return false
    }

    return [
      ...this.getBetweenTinselCurve(branch1, intersectBranch, otherBranches, clockwise1, clockwise2, depths, loopRadius, 0),
      this.getOverTinselCurve(intersectBranch, clockwise1, loopRadius),
      ...this.getBetweenTinselCurve(intersectBranch, branch2, otherBranches, !clockwise2, clockwise2, depths, loopRadius, 0)
    ]
  }

  getOverTinselCurve (branch, clockwise, loopRadius) {
    return getBranchCurve(branch, branch, !clockwise, clockwise, -loopRadius, loopRadius)
  }

  createTinselCurvePath (curveDefinitions, loopRadius) {
    const curvePath = new THREE.CurvePath()
    const branches = this.store.getters.branches
    const curveFunctions = {
      over: x => this.getOverTinselCurve.bind(this)(branches[x.branchIndex], x.clockwise, loopRadius),
      between: x => this.getBetweenTinselCurve.bind(this)(branches[x.branches[0].branchIndex], branches[x.branches[1].branchIndex], branches, x.branches[0].clockwise, x.branches[1].clockwise, getDepthsToTry(x.depth), loopRadius, 0)
    }
    const curves = curveDefinitions.map(definition => curveFunctions[definition.curveType](definition)).flat()
    curves.forEach(c => curvePath.add(c))
    return curvePath
  }

  createTinsel (tinsel) {
    const tinselType = tinselTypes[tinsel.typeIndex]
    const curvePaths = tinsel.curves.map(curves => this.createTinselCurvePath(curves, tinselType.loopRadius))
    const tinselGroup = makeGroupUnder(tinsel.id, this.tinsels)
    const meshGroup = makeGroupUnder('geometry', tinselGroup)

    tinselType.geometry.forEach((x, i) => {
      const space = (1 / tinselType.geometry.length) * i
      const mesh = curveToGeometry(tinselType.interval, curvePaths, x, tinselType.material[i], this.eye, this.lookAt, space)
      meshGroup.add(mesh)
    })

    if (tinselType.wire) {
      curvePaths.forEach(curvePath => {
        meshGroup.add(curvePathToTube(curvePath, this.materials.wire, 0.003))
      })
    }

    curvePaths.forEach((curvePath, i) => {
      const selected = tinsel.selected[i]
      const highlight = this.selectionCurve(curvePath, selected)
      highlight.name = tinsel.ids[i]
      this.tinselHighlights.add(highlight)
    })

    curvePaths.forEach(curvePath => {
      dispose(...curvePath.curves)
      dispose(curvePath)
    })
  }

  removeTinsel (id) {
    const obj = this.tinsels.getObjectByName(id)
    // TODO make recursive remove and dispose function
    this.tinsels.remove(obj)
    dispose(obj)
  }

  removeHiglights (ids) {
    ids.forEach(id => {
      const obj = this.tinselHighlights.getObjectByName(id)
      this.tinselHighlights.remove(obj)
      dispose(obj)
    })
  }

  removeBauble (name) {
    const toRemove = this.baubles.getObjectByName(name)
    this.baubles.remove(toRemove)
    dispose(toRemove)
  }

  highlightTinsel (id) {
    this.tinselHighlights.children.forEach(highlight => {
      const selected = highlight.name === id
      highlight.material = selected ? this.materials.highlight : this.materials.hiddenHighlight
      highlight.needsUpdate = true
    })
    this.animate()
  }
}
