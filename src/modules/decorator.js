import { lastOf } from './utils.js'

/* modes:
  add-bauble
  remove-bauble
  add-tinsel
  select-tinsel
*/

const getInitialState = () => ({
  currentTinselPieceIndex: 0,
  mode: 'add-bauble',
  currentBaubleTypeIndex: 0
})

const decorator = {
  state: { ...getInitialState() },
  mutations: {
    setMode (state, payload) {
      state.mode = payload
    },
    setCurrentBaubleTypeIndex (state, payload) {
      state.currentBaubleTypeIndex = payload
    },
    selectTinsel (state, payload) {
      state.currentTinselPieceIndex = payload
    },
    endTinsel (state) {
      state.currentTinselPieceIndex = state.currentTinselPieceIndex + 1
    }
  },
  getters: {
    tinselIndexFromId: (state, getters) => (id) => getters.tinselCurvesWithIds.findIndex(x => x.id === id),
    currentBaubleType: (state, getters, rootState, rootGetters) => {
      return rootState.decorationTypes.baubles[state.currentBaubleTypeIndex]
    },
    currentTinselPiece: (state, getters, rootState, rootGetters) => {
      return rootState.decorations.tinsels[state.currentTinselPieceIndex]
    },
    currentTinselType: (state, getters, rootState) => {
      return rootState.decorationTypes.tinsels[getters.currentTinselPiece.typeIndex]
    },
    numTinsels: (state, getters, rootState) => {
      return rootState.decorations.tinsels.length
    },

    baubleIndexFromBranchIndex: (state, getters, rootState) => (branchIndex) => rootState.decorations.baubles.findIndex(x => x.branchIndex === branchIndex),
    tinselIndexToHighlight: state => ['add-tinsel', 'select-tinsel'].includes(state.mode) ? state.currentTinselPieceIndex : false
  },
  actions: {
    touchBranchBaubleMode (context, branchIndex) {
      const baubles = context.rootState.decorations.baubles
      const selectionBaubleIndex = baubles.findIndex(x => x.branchIndex === branchIndex)
      if (selectionBaubleIndex > -1) {
        if (baubles[selectionBaubleIndex].typeIndex === context.state.currentBaubleTypeIndex) {
          context.commit('removeBauble', selectionBaubleIndex)
        } else {
          context.commit('changeBauble', { index: selectionBaubleIndex, typeIndex: context.state.currentBaubleTypeIndex })
        }
      } else {
        context.commit('addBauble', { branchIndex, typeIndex: context.state.currentBaubleTypeIndex })
      }
    },
    touchBranchTinselMode (context, branchIndex) {
      const currentTinselPiece = context.getters.currentTinselPiece
      if (!currentTinselPiece) {
        context.commit('createTinselPiece', {
          branchIndex,
          typeIndex: context.getters.currentTinselPiece.typeIndex,
          depth: context.getters.currentTinselPiece.depth
        })
      } else {
        if (branchIndex !== lastOf(currentTinselPiece.branchIndices)) {
          context.commit('addTinsel', { branchIndex, tinselIndex: context.state.currentTinselPieceIndex })
        }
      }
    },
    touchBranch (context, branchIndex) {
      switch (context.state.mode) {
        case 'add-bauble':
          context.dispatch('touchBranchBaubleMode', branchIndex)
          break
        case 'add-tinsel':
          context.dispatch('touchBranchTinselMode', branchIndex)
          break
        default:
          console.log('unknown mode')
      }
    },
    removeLastTinselPiece (context) {
      const arr = context.getters.currentTinselPiece.branchIndices
      if (arr.length > 1 || context.state.currentTinselPieceIndex === 0) {
        context.commit('removeLastTinselPiece', context.state.currentTinselPieceIndex)
      } else {
        context.commit('removeTinsel', context.state.currentTinselPieceIndex)
        context.commit('selectTinsel', context.state.currentTinselPieceIndex - 1)
      }
    },
    removeSelectedTinsel (context) {
      if (context.state.currentTinselPieceIndex === 0) {
        context.commit('createEmptyTinselPiece', {
          typeIndex: context.getters.currentTinselPiece.typeIndex,
          depth: context.getters.currentTinselPiece.depth,
          branchIndices: []
        })
      }

      context.commit('removeTinsel', context.state.currentTinselPieceIndex)
      context.commit('selectTinsel', context.rootState.decorations.tinsels.length - 1)
    },
    removeBauble (context, branchIndex) {
      const index = context.getters.baubleIndexFromBranchIndex(branchIndex)
      if (index > -1) {
        context.commit('removeBauble', index)
      }
    },
    selectTinsel (context, id) {
      context.dispatch('clearEmptyTinsels')
      const tinselIndex = context.getters.tinselIndexFromId(id)
      context.commit('selectTinsel', tinselIndex)
      context.commit('setMode', 'add-tinsel')
    },
    rotateBaubleType (context, payload) {
      const n = context.rootGetters.totalBaubleTypes
      const curr = context.state.currentBaubleTypeIndex
      const newIndex = (curr + n + payload) % n
      context.commit('setCurrentBaubleTypeIndex', newIndex)
    },
    rotateTinselType (context, payload) {
      const n = context.rootGetters.totalTinselTypes
      const curr = context.getters.currentTinselPiece.typeIndex
      const newIndex = (curr + n + payload) % n
      context.commit('setTinselType', { tinselIndex: context.state.currentTinselPieceIndex, typeIndex: newIndex })
    },
    changeTinselDepth (context, payload) {
      context.commit('changeTinselDepth', { tinselIndex: context.state.currentTinselPieceIndex, change: payload })
    },
    reverseTinsel (context, payload) {
      context.commit('reverseTinsel', context.state.currentTinselPieceIndex)
    },
    newTinsel (context, payload) {
      context.commit('setMode', 'add-tinsel')
      if (context.getters.currentTinselPiece.branchIndices.length > 0) {
        context.commit('createEmptyTinselPiece', {
          typeIndex: context.getters.currentTinselPiece.typeIndex,
          depth: context.getters.currentTinselPiece.depth
        })
      }
      context.commit('selectTinsel', context.getters.numTinsels - 1)
    },
    resetDecorations (context, payload) {
      context.commit('resetDecorations', {
        typeIndex: context.getters.currentTinselPiece.typeIndex,
        depth: context.getters.currentTinselPiece.depth
      })
      context.commit('selectTinsel', 0)
    }
  }
}

export default decorator
