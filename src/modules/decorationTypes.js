import baubleTypesData from '../assets/baubleTypes.json'
import tinselTypesData from '../assets/tinselTypes.json'

const decorationTypes = {
  state: {
    baubles: baubleTypesData,
    tinsels: tinselTypesData
  },
  getters: {
    totalTinselTypes: state => state.tinsels.length,
    totalBaubleTypes: state => state.baubles.length
  }
}

export default decorationTypes
