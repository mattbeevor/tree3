import { createPine } from '@/treeGetters.js'

function arrayIsCloseToArray (arr1, arr2) {
  arr1.forEach((x, i) => { expect(x).toBeCloseTo(arr2[i], 5) })
}

test('rotation test 1', () => {
  expect(createPine(1)).toBeCloseTo(1)
})
