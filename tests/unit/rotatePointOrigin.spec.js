/* import { shallowMount } from '@vue/test-utils'
import HelloWorld from '@/components/HelloWorld.vue'

describe('HelloWorld.vue', () => {
  it('renders props.msg when passed', () => {
    const msg = 'new message'
    const wrapper = shallowMount(HelloWorld, {
      propsData: { msg }
    })
    expect(wrapper.text()).toMatch(msg)
  })
}) */

import { rotatePointOrigin } from '@/treeGetters.js'

function arrayIsCloseToArray (arr1, arr2) {
  arr1.forEach((x, i) => { expect(x).toBeCloseTo(arr2[i], 5) })
}

test('rotation test 1', () => {
  arrayIsCloseToArray(rotatePointOrigin([0, 0, 1], 'y', 90), [-1, 0, 0])
})

test('rotation test 2', () => {
  arrayIsCloseToArray(rotatePointOrigin([1, 0, 0], 'y', 90), [0, 0, 1])
})

test('rotation test 3', () => {
  arrayIsCloseToArray(rotatePointOrigin([1, 0, 0], 'y', -90), [0, 0, -1])
})

test('rotation test 4', () => {
  arrayIsCloseToArray(rotatePointOrigin([1, 0, 0], 'y', -180), [-1, 0, 0])
})

test('rotation test 5', () => {
  arrayIsCloseToArray(rotatePointOrigin([0, 0, -1], 'y', -180), [0, 0, 1])
})

test('rotation test 6', () => {
  arrayIsCloseToArray(rotatePointOrigin([0, 1, 0], 'y', -180), [0, 1, 0])
})

test('rotation test 7', () => {
  arrayIsCloseToArray(rotatePointOrigin([12, 16, 10], 'y', 0), [12, 16, 10])
})
